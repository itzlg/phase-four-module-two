package com.fishleap.repository;

import com.fishleap.entity.COrder;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author zlg
 */
public interface COrderRepository extends JpaRepository<COrder, Long> {
}
