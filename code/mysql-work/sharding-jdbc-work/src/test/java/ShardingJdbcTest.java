import com.fishleap.Runboot;
import com.fishleap.entity.COrder;
import com.fishleap.repository.COrderRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Repeat;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.Random;

/**
 * @author zlg
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Runboot.class)
public class ShardingJdbcTest {

    @Resource
    private COrderRepository orderRepository;

    @Test
    @Repeat(10)
    public void addTest() {
        Random random = new Random();
        int userId = random.nextInt(1000);
        COrder order = new COrder();
        order.setDel(false);
        order.setCompanyId(666666);
        order.setPositionId(88888888);
        order.setUserId(userId);
        order.setPublishUserId(666);
        order.setResumeType(888);
        order.setStatus("AUTO");
        order.setCreateTime(new Date());
        order.setUpdateTime(new Date());
        orderRepository.save(order);
    }

    @Test
    public void findTest() {
        List<COrder> list = orderRepository.findAll();
        System.out.println(list.toString());
    }


}
